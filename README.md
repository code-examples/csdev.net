# [csdev.net](https://csdev.net) source codes

<br/>

### Run csdev.net on localhost

    # vi /etc/systemd/system/csdev.net.service

Insert code from csdev.net.service

    # systemctl enable csdev.net.service
    # systemctl start csdev.net.service
    # systemctl status csdev.net.service

http://localhost:4034
