---
layout: null
permalink: robots.txt
---

User-agent: *
Allow: /
Host: https://csdev.net
Sitemap: https://csdev.net/sitemap.xml
