---
layout: page
title: C# Video Courses on English
description: C# Video Courses on English
keywords: Программирование, C#, .Net, английский язык
permalink: /materials/en/
---

# C# Video Courses on English

### Visual Studio Online for Beginners

<br/><br/>

<div align="center">
	<iframe width="853" height="480" src="https://www.youtube.com/embed/BaEiIvbaq-E" frameborder="0" allowfullscreen></iframe>
</div>

<br/><br/>

<hr/>
<br/><br/>

### Developer Productivity: What's New in C# 6.0

<br/><br/>

<div align="center">
	<iframe width="853" height="480" src="https://www.youtube.com/embed/1ynbHTalzVk" frameborder="0" allowfullscreen></iframe>
</div>

<br/><br/>

<hr/>
<br/><br/>

### Derek Banas C# Tutorial

<br/><br/>

<div align="center">
	<iframe width="853" height="480" src="https://www.youtube.com/embed/lisiwUZJXqQ" frameborder="0" allowfullscreen></iframe>
</div>

<br/><br/>

<hr/>
<br/><br/>

### Asp.Net Web API Design Course ~5 Hours

<br/><br/>

<div align="center">
	<iframe width="853" height="480" src="https://www.youtube.com/embed/YFPRm58UtIo" frameborder="0" allowfullscreen></iframe>
</div>

<br/><br/>

<hr/>
<br/><br/>

### Real-Time Web Apps with Asp.Net SignalR

<br/><br/>

<div align="center">
	<iframe width="853" height="480" src="https://www.youtube.com/embed/kyFBgephmpQ" frameborder="0" allowfullscreen></iframe>
</div>

<br/><br/>

<hr/>
<br/><br/>

### .NET Core API Review 2015

<br/><br/>

<div align="center">
	<iframe width="853" height="480" src="https://www.youtube.com/embed/ozbpzIqQxNg" frameborder="0" allowfullscreen></iframe>
</div>

<br/><br/>

<hr/>
<br/><br/>

### Git Training for the .NET Team

<br/><br/>

<div align="center">
	<iframe width="853" height="480" src="https://www.youtube.com/embed/OOiaOsuKnrc" frameborder="0" allowfullscreen></iframe>
</div>

<br/><br/>

<hr/>
<br/><br/>

### ASP.NET MVC & Entity Framework - Step by Step Video Tutorial

<br/><br/>

<div align="center">

<iframe width="853" height="480" src="//www.youtube.com/embed/VAtVv1Q7ufM" frameborder="0" allowfullscreen></iframe>

</div>
